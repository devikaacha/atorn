<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Admin;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Mail;
 
 
class AdminLoginController extends Controller
{
    //Admin Login
    public function adminLogin(){
        if(Auth::guard('admin')->check()){
            return redirect('/admin/dashboard');

        }else{

            return view('admin.auth.login');
        }
        
    }
     
    public function loginAdmin(Request $request){
        $data=$request->all();
        $rules =[
            'email' =>'required|email|max:255',
            'password' =>'required'
        ];
        $customMessages =[
            'email.required' => 'E-Mail Address is required',
            'email.email' => 'Please enter a valid email address',
            'email.max' => 'You are not allowed to enter more than 255 characters',
            'password.required' => 'Password required',
        ];
        $this->validate($request, $rules, $customMessages);
        if(Auth::guard('admin')->attempt(['email'=> $data['email'], 'password' =>$data['password']])){
            return redirect('/admin/dashboard');

        }else{
            Session::flash('error_message', 'Invalid Email or Password');

            return redirect()->back();

        }

    }
    //admin Dashboard
    public function adminDashboard(){
        return view('admin.dashboard');
    }

    //admin logout
    public function adminLogout(){
        Auth::guard('admin')->logout();
        Session::flash('info_message', 'Logout Successful');
        return redirect('/admin/login');

    }

    //Admin Reset Password
    public function adminResetPassword(){
        return view('admin.auth.reset');
    }

    public function adminResetPw(Request $request){
    if($request->isMethod('post')){
        $data=$request->all();
        $rules =[
            'email' =>'required|email|max:255',
            
        ];
        $customMessages =[
            'email.required' => 'E-Mail Address is required',
            'email.email' => 'Please enter a valid email address',
            'email.max' => 'You are not allowed to enter more than 255 characters',
            
        ];
        $this->validate($request, $rules, $customMessages);
        $adminCount=Admin::where('email',$data['email'])->count();
        if($adminCount == 0){
            return redirect()->back()->with('error_message', 'E-Mail Address Does Not Exists in our database');
           }
        //Get Admin Details
        $adminDetails=Admin::where('email',$data['email'])->first();
        //Generate New Password
        $random_password=str::random(8);
        //Encode Password
        $new_password=bcrypt($random_password);
        //Update Password
        Admin::where('email',$data['email'])->update(['password'=>$new_password]);

        //send email for the code
        $email=$data['email'];
        $name=$adminDetails->name;
        $messageData=['email'=>$email, 'password'=>$random_password, 'name'=>$name];
        Mail::send('emails.forgetpassword', $messageData, function($message)use($email){
            $message->to($email)->subject('New Password-Atorn');
        });

        Session::flash('error_message', 'Password has been changed.please check your email address for new password');
        return redirect()->back();
     }
    }
}