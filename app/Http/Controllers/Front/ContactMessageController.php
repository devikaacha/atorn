<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\ContactMessage;

class ContactMessageController extends Controller
{
    //Store Message 
    public function contactMessage(Request $request){
        $data=$request->all();
        $rules = [
            'name' => 'required|max:255',
            'email'=>'required',
            'phone'=>'required',
            'subject'=>'required',
            'contact_message'=>'required',
        ];
        $customMessages = [
            'name.required' => 'Name is required',
            'name.max' => 'name must not be more than 255 characters',
            'email.required' => 'email is required',
            'phone.required' => 'phone is required',
            'contact_message.required' => 'contact_message is required',
        ];
        $this->validate($request, $rules, $customMessages);
        $c_message= new ContactMessage();
        $c_message->name=$data['name'];
        $c_message->email=$data['email'];
        $c_message->phone=$data['phone'];
        $c_message->subject=$data['subject'];
        $c_message->contact_message=$data['contact_message'];
        $c_message->save();

        $notification = array(
            'alert-type' => 'success',
            'message' => 'Thank You for contacting us.We will get back to you soon'
          );
          return redirect()->back()->with($notification);
        



    }
}
