@extends('admin.includes.admin_design')

@section('site_title')
   Edit Page- {{$themes->website_name}}
@endsection

@section('content')
 <!--start content-->
 <main class="page-content">





<div class="row">
  <div class="col-12 col-lg-12">
    <div class="card shadow-sm border-0">
      <div class="card-body">
          <h5 class="mb-0">Edit Page</h5>
          <hr>
          <div class="card shadow-none border">

          @include('admin.includes._message')
            
            <div class="card-body">
              <form class="row g-3" method="post" action="{{route('page.update',$page->id)}}" enctype="multipart/form-data">
                @csrf
                 <div class="col-6">
                    <label for ="page_name" class="form-label">Page Name<span class="text-danger">*</label>
                    <input type="text" class="form-control" value="{{$page->page_name}}" name="page_name" id="page_name">
                 </div>

                 <div class="col-6">
                    <label for ="slug" class="form-label">Slug<span class="text-danger">*</label>
                    <input disabled type="text" class="form-control" value="{{$page->slug}}" name="slug" id="slug">
                 </div>

                
                 <div class="col-6">
                    <label for ="thumbnail_image" class="form-label">Image</label>
                    <input type="file" class="form-control" value="" name="thumbnail_image" id="thumbnail_image" accept="image/*" onchange="readURL(this)">
                 </div>

                 <div class="col-6"></div>
                 @if(!empty($page->thumbnail_image))
                 <img id="one" src="{{asset('public/uploads/'.$page->thumbnail_image)}}" alt="" style="width: 300px !important;">
                 @endif
                 <h5>SEO Information</h5>

                 <div class="col-6">
                    <label for ="seo_title" class="form-label">Seo Title</label>
                    <input type="text" class="form-control" value="{{$page->seo_title}}" name="seo_title" id="seo_title">
                 </div>

                 <div class="col-6">
                    <label for ="seo_subtitle" class="form-label">Seo SubTitle</label>
                    <input type="text" class="form-control" value="{{$page->seo_subtitle}}" name="seo_subtitle" id="seo_subtitle">
                 </div>

                 <div class="col-12">
                    <label for ="seo_description" class="form-label">Seo Description</label>
                    <input type="text" class="form-control" value="{{$page->seo_description}}" name="seo_description" id="seo_description">
                 </div>

                 <div class="col-12">
                    <label for ="seo_keyword" class="form-label">Seo Keyword</label>
                    <input type="text" class="form-control" value="{{$page->seo_keyword}}" name="seo_keyword" id="seo_keyword">
                 </div>

                
                       

                 <div class="text-start">
                    <button type="submit" class="btn btn-success px-4">Update Information</button>
                </div>
              </form>
            </div>
          </div>
          
          
      </div>
    </div>
  </div>

</div><!--end row-->

</main>
<!--end page main-->
@endsection

@section('js')

<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>

<script>
  $(function() {
    $('#status').bootstrapToggle({
      on: ' Mark as Active',
      off: 'Mark as In Active'
    });
  })
</script>


<!-- include summernote css/js -->
<link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.css" rel="stylesheet">
<script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.js"></script>

<script>
$(document).ready(function() {
 $('#details').summernote();
 height:100
});
 </script>

<script type="text/javascript">
        function readURL(input){
            if(input.files && input.files[0]){
                var reader = new FileReader();
                reader.onload = function (e){
                    $("#one").attr('src', e.target.result).width(300);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }
    </script>
   

 @endsection