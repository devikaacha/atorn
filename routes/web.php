<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//index/front page
Route::get('/',[\App\Http\Controllers\Front\IndexController::class,'index'])->name('index');
//contactus page
Route::get('/contact-us',[\App\Http\Controllers\Front\FrontEndController::class,'contactUs'])->name('contactUs');

//Contact Message
Route::post('/contact/message',[\App\Http\Controllers\Front\ContactMessageController::class,'contactMessage'])->name('contactMessage');

Route::prefix('/admin')->group(function(){

    //Admin Login
Route::get('login/',[\App\Http\Controllers\Admin\AdminLoginController::class,'adminlogin'])->name('adminLogin');
Route::post('login/',[\App\Http\Controllers\Admin\AdminLoginController::class,'loginAdmin'])->name('loginAdmin');

Route::group(['middleware'=> 'admin'], function(){
    //Admin Dashboard
     Route::get('dashboard/',[\App\Http\Controllers\Admin\AdminLoginController::class,'adminDashboard'])->name('adminDashboard');    

    //Admin Profile
    Route::get('/profile',[\App\Http\Controllers\Admin\AdminProfileController::class,'adminProfile'])->name('adminProfile');    
    
    //Admin Profile Update
    Route::post('/profile/update/{id}',[\App\Http\Controllers\Admin\AdminProfileController::class,'adminProfileUpdate'])->name('adminProfileUpdate');
    //Delete Image
    Route::get('/delete-image/{id}',[\App\Http\Controllers\Admin\AdminProfileController::class,'deleteImage'])->name('deleteImage');
      
    //change password
    Route::get('/change-password',[\App\Http\Controllers\Admin\AdminProfileController::class,'changePassword'])->name('changePassword');
   //Check Current Password
   Route::post('/check-password',[\App\Http\Controllers\Admin\AdminProfileController::class,'chkUserPassword'])->name('chkUserPassword');



   //Theme Setting
   Route::get('/theme',[\App\Http\Controllers\Admin\ThemeController::class,'theme'])->name('theme');
   Route::post('/theme/{id}',[\App\Http\Controllers\Admin\ThemeController::class,'themeUpdate'])->name('themeUpdate');
   Route::get('/social',[\App\Http\Controllers\Admin\ThemeController::class,'social'])->name('social');
   Route::post('/social/update/{id}',[\App\Http\Controllers\Admin\ThemeController::class,'socialUpdate'])->name('socialUpdate');
   //Banner Route
   Route::get('/banners',[\App\Http\Controllers\Admin\BannerController::class,'index'])->name('banner.index');
   Route::get('/banners/add',[\App\Http\Controllers\Admin\BannerController::class,'add'])->name('banner.add');
   Route::post('/banners/store',[\App\Http\Controllers\Admin\BannerController::class,'store'])->name('banner.store');
   Route::get('/table/banner',[\App\Http\Controllers\Admin\BannerController::class,'dataTable'])->name('table.banner');
   Route::get('/changeStatus',[\App\Http\Controllers\Admin\BannerController::class,'changeStatus'])->name('changeStatus');
   Route::get('/banners/show/{id}',[\App\Http\Controllers\Admin\BannerController::class,'show'])->name('banner.show');
   Route::get('/banners/edit/{id}',[\App\Http\Controllers\Admin\BannerController::class,'edit'])->name('banner.edit');
   Route::post('/banners/update/{id}',[\App\Http\Controllers\Admin\BannerController::class,'update'])->name('banner.update');
   Route::get('/delete-banner/{id}', [\App\Http\Controllers\Admin\BannerController::class, 'delete'])->name('banner.delete');

   //Pages CRUD
   Route::get('/pages', [\App\Http\Controllers\Admin\PageController::class, 'index'])->name('page.index');
   Route::get('/page/add', [\App\Http\Controllers\Admin\PageController::class, 'add'])->name('page.add');
   Route::post('/page/store', [\App\Http\Controllers\Admin\PageController::class, 'store'])->name('page.store');
   Route::get('/table/page',[\App\Http\Controllers\Admin\PageController::class,'dataTable'])->name('table.page');
   Route::get('/page/edit/{id}', [\App\Http\Controllers\Admin\PageController::class, 'edit'])->name('page.edit');
   Route::post('/page/update/{id}', [\App\Http\Controllers\Admin\PageController::class, 'update'])->name('page.update');
   Route::get('/delete-page/{id}', [\App\Http\Controllers\Admin\PageController::class, 'delete'])->name('page.delete');
   


});



Route::get('logout/',[\App\Http\Controllers\Admin\AdminLoginController::class,'adminLogout'])->name('adminLogout');

//Reset Password
Route::get('/reset/password',[\App\Http\Controllers\Admin\AdminLoginController::class,'adminResetPassword'])->name('adminResetPassword');
Route::post('/reset/password',[\App\Http\Controllers\Admin\AdminLoginController::class,'adminResetPw'])->name('adminResetPw');

});
