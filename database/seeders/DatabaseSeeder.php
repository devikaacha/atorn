<?php

namespace Database\Seeders;

use App\Models\Admin;
use Illuminate\Database\Seeder;
use App\Models\Theme;
use App\Models\Social;


class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();

        Admin::insert([
           'name' => 'Devika Acharya',
           'email' => 'joniacharya81@gmail.com',
           'password' => bcrypt('password'),

        ]);

        Admin::insert([
            'name' => 'Manish Gautam',
            'email' => 'manish@gmail.com',
            'password' => bcrypt('password'),
 
         ]);

         Theme::insert([
            'website_name'=>'Tech Coderz',
            'website_tagline'=>'Inspire the Next',
            'favicon'=>''
         ]);

         Social::insert([
            'facebook'=>'',
         ]);
    }
}
